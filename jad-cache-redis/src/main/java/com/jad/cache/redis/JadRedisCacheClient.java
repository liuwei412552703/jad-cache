package com.jad.cache.redis;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;

import com.jad.cache.common.AbstractCacheClient;
import com.jad.cache.common.JadCacheManager;

public class JadRedisCacheClient extends AbstractCacheClient  {

	@SuppressWarnings("rawtypes")//
	private RedisTemplate template;
	
	private static final String CACHE_MANAGER_IMPL_NAME="cacheManagerImpl";
	
//	public JadRedisCacheClient(){
//		JadRedisCacheManager manager = new JadRedisCacheManager();
//		manager.setCacheClient(this);
////		manager.setTemplate(template);
//		
//		this.setCacheManager(manager);
//		
//		registryToMasterCacheManager(this.getCacheManager());
//	}
	
	@Override
	protected void registryCacheManager(BeanDefinitionRegistry registry) {
		
		String cacheManagerBean=this.getClientName()+"_" + CACHE_MANAGER_IMPL_NAME;
		if (!registry.containsBeanDefinition(cacheManagerBean)) {
			Assert.notNull(template, "没有注入RedisTemplate");
			RootBeanDefinition beanDefinition=new RootBeanDefinition(JadRedisCacheManager.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			beanDefinition.getPropertyValues().add("cacheClient", this);
			beanDefinition.getPropertyValues().add("template", template );
			registry.registerBeanDefinition(cacheManagerBean, beanDefinition);
		}
		JadCacheManager manager = (JadCacheManager)applicationContext.getBean(cacheManagerBean);
		this.setCacheManager(manager);
		registryToMasterCacheManager(manager);//注册到主管理器
		
		
		
	}
	

	@SuppressWarnings("rawtypes")
	public RedisTemplate getTemplate() {
		return template;
	}

	@SuppressWarnings("rawtypes")
	public void setTemplate(RedisTemplate template) {
		this.template = template;
	}
	
	

}
