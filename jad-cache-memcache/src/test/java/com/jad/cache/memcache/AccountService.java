package com.jad.cache.memcache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

	
	private static Logger logger=LoggerFactory.getLogger(AccountService.class); 
	
	
//	 @Cacheable(value="accountCache",condition="#userName.length() <= 4")// 缓存名叫 accountCache
	 @Cacheable(value="accountCache1")// 缓存名叫 accountCache
	 public Account getAccountByName(String userName) {
		 // 方法内部实现不考虑缓存逻辑，直接实现业务
		 return getFromDB(userName); 
	 }
	 
	 @Cacheable(value="masterCache2")// 缓存名叫 accountCache
	 public Account getAccountByName2(String userName) {
		 // 方法内部实现不考虑缓存逻辑，直接实现业务
		 return getFromDB(userName); 
	 }

	 public Account getFromDB(String userName) {
		 logger.debug("从数据库中查询account");
		 return new Account();
//		 return null;
		 
	 }

}
