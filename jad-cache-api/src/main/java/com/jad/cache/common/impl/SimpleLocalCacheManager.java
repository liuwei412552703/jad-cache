package com.jad.cache.common.impl;

import java.util.Collection;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.springframework.cache.Cache;
import org.springframework.util.Assert;

import com.jad.cache.common.JadAbstractCacheManager;
import com.jad.cache.common.JadCache;

/**
 * 简单的本地缓存管理器
 *
 */
public class SimpleLocalCacheManager extends JadAbstractCacheManager {
	
	private static Logger logger = Logger.getLogger(SimpleLocalCacheManager.class);
	
	private Collection<? extends Cache> caches;

	@Override
	protected Collection<? extends Cache> loadCaches() {
		if(this.caches == null){
			this.caches = new HashSet();
		}
		return this.caches;
	}
	
	protected Cache getMissingCache(String name) {
		
		if(!this.getCacheClient().isAutoCreateCache()){
			logger.warn("当前缓存客户端不允许自动创建缓存,"+this.getLoggerInfo(name));
			return null;
		}
		
		logger.debug("没有找到缓存，自动创建一个,"+this.getLoggerInfo(name));
		
		
		
		return newCache(name);
	}
	
	public Cache newCache(JadCache jc){
		Assert.notNull(this.getCacheClient(),"创建缓存失败,没有注入缓存客户端,"+this.getLoggerInfo(jc.getName()));
		return new LocalMapCache(this.getCacheClient(),jc.getName(),jc.getDefActivityTime(), jc.isAllowNullValues());
	}
	
	
	public Cache newCache(String name){
		Assert.notNull(this.getCacheClient(),"创建缓存失败,没有注入缓存客户端,"+this.getLoggerInfo(name));
		int activityTime =  getCacheClient().getDefActivityTime() == null? 0 : getCacheClient().getDefActivityTime().intValue();
		return new LocalMapCache(this.getCacheClient(),name,activityTime, getCacheClient().isAllowNullValues());
	}
	


}



