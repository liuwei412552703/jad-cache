package com.jad.cache.common;

import java.util.Map;

/**
 * 客户端管理器
 * 
 * 管理整个应用中所有缓存客户端，在整个应用系统中，它应该是一个单例
 * 这个类在初始化的同时，也会初始化一个MasterCacheManager实例，用于管理所有被CacheClient控制的CacheManager
 *  
 * jad-cache 缓存框架中，有两个此类的实现类：
 * 一个是只能管理一个客户端的 SingleClientManager，用于在系统使用单一缓存厂商的环境下
 * 一个是可同时管理多个客户的 MultiClientManager，同于可同时使用ehcache,memcache等多种缓存厂商的业务环境
 *
 */
public interface CacheClientManager {

	/**
	 * 获得缓存客户端
	 * @return
	 */
	public CacheClient getCacheClient(String clientName);
	
	/**
	 * 获得被管理的客户端列表
	 * @return
	 */
	public Map<String,CacheClient> getCacheClients();
	
	/**
	 * 缓存客户端启动
	 */
	public void start(CacheClient client);
	
	/**
	 * 缓存客户端停止
	 */
	public void stop(CacheClient client);
	
	/**
	 * 是否启动
	 * @return
	 */
	public boolean isStarted(CacheClient client) ;

	
	/**
	 * 对像默认存活时间
	 * 以秒为单位
	 * 负数或0表示不限制
	 * @return
	 */
	public Integer getDefActivityTime() ;
	
	/**
	 * 是否自动创建缓存
	 * @return
	 */
	public boolean isAutoCreateCache();
	
	/**
	 * 能否缓存null
	 * @return
	 */
	public boolean isAllowNullValues();
	
}





