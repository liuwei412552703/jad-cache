package com.jad.cache.common.api;

import java.util.Collection;

import com.jad.cache.common.CacheClient;
import com.jad.cache.common.CacheException;
import com.jad.cache.common.MasterCacheManager;

/**
 * 客户端操作工具类
 *
 */
public class CacheClientHelper {
	
	
	
	/**
	 * 主缓存缓存管理器
	 */
	private static MasterCacheManager MASTER_CACHE_MANAGER;
	
	/**
	 * 是否初始化
	 */
	private static volatile boolean INITIATED = false ;
	
	/**
	 * 当前类的实例
	 */
	private static final CacheClientHelper INSTANCE = new CacheClientHelper();
	
	/**
	 * 私人化构造函数
	 */
	private CacheClientHelper(){
	}
	
	/**
	 * 获取当前类的实例
	 * @return
	 */
	public static CacheClientHelper getInstance(){
		assertInited();
		return INSTANCE;
	}
	
	/**
	 * 初始化
	 * @param masterCacheManager
	 */
	public static void init(MasterCacheManager masterCacheManager){
		MASTER_CACHE_MANAGER = masterCacheManager;
		INITIATED = true;
	}
	
	/**
	 * 启动客户端
	 * @param clientName
	 */
	public void start(String clientName){
		getClient(clientName).start();
	}
	
	/**
	 * 停此客户端
	 * @param clientName
	 */
	public void stop(String clientName){
		getClient(clientName).stop();
	}
	
	/**
	 * 是否启动
	 * @param clientName
	 * @return
	 */
	public boolean isStarted(String clientName) {
		return getClient(clientName).isStarted();
	}
	
	/**
	 * 获得所有客户端名称
	 * @return
	 */
	public Collection<String>getClientNames(){
		return MASTER_CACHE_MANAGER.getClientNames();
	}
	
	/**
	 * 通过名称获取CacheClient实例
	 * @param clientName
	 * @return
	 */
	private CacheClient getClient(String clientName){
		return MASTER_CACHE_MANAGER.getClient(clientName);
	}
	
	/**
	 * 确保已经初始化
	 */
	private static void assertInited(){
		if(!INITIATED){
			throw new CacheException("缓存还没有初始化");
		}
	}
	
	
}












